import React, {useState} from 'react';
import './App.css';

const REGEX_FRIENDS = /^([A-z]+)( est amie? avec )([A-z]+)$|^([A-z]+)( suis ami avec )([A-z]+)$/
const REGEX_QUEST = /^(Est-ce que )([A-z]+)( est mon amie? \?)$/

/**
 *
 * @type {{}}
 */
let user_db = {};
let groups_db = [];

function App() {
    let [prompt, setPrompt] = useState(`Benjamin est ami avec Paul
Sophie est amie avec moi
Je suis ami avec Benjamin
---
Est-ce que Sophie est mon amie ?`);
    let [hasError, setHasError] = useState(false);
    let [groups, setGroups] = useState([]);

    function updateFront() {
        user_db = {};
        groups_db = [];
        setGroups([]);

        prompt.trim();
        let [friends, question] = prompt.split('---');

        /**
         * Sépare les lignes grâce aux sauts de ligne
         * @type {string[]}
         */
        friends = friends.split('\n').filter(string => {
            return string !== ""
        } );

        /**
         * Chaque ligne est interprétée avec les groupes REGEX, Deux utilisateurs bientôt amis en sont déduits.
         */
        let future_friends = [];
        friends.forEach((string) => {
            let [fullStr, user1, l, user2 ] = string.match(REGEX_FRIENDS).filter(string => {return string !== undefined});
            if(!user1 || !user2) setHasError(true);
            else{

                future_friends.push({user1: ru(user1), user2: ru(user2)});
            }
        })

        /**
         * on simule une db async..
         * @type {*[]}
         */
        let promises = [];
        if(!hasError){
            future_friends.forEach((object)=>{
                promises.push(updateDb(object.user1.toLowerCase(), object.user2.toLowerCase()));
            })

            Promise.all(promises).then(()=>{
                /**
                 * Traitement de la question
                 */

                let user = question.trim().match(REGEX_QUEST)[2];
                if(!user) setHasError(true)
                else{
                    setGroups(groups_db);
                    alert(user_db['moi'].group === user_db[user.toLowerCase()].group)
                }
            })
        }
    }

    return (
        <div>
            <div style={{width: "100%"}}>
                <div>
                    <textarea
                        value={prompt}
                        onChange={(e) => setPrompt(e.target.value)}/>

                    <button onClick={updateFront}>Valider</button>
                </div>
            </div>

            <div style={{display: "flex"}} xs={3}>
                {
                    groups.map((group, index)=>{
                        return <div style={{width: "33%", border: "1px solid black", borderRadius: "3px"}}>
                            <h3>Groupe n°{index +1}</h3>
                            {
                                group.map((user)=>{
                                    return <div>{user}</div>
                                })
                            }
                        </div>
                    })
                }
            </div>
        </div>
    );
}

export default App;

/**
 * Refactor user pour eviter d'avoir 2 utilisateurs pour je & moi
 * @param name
 * @returns {string|*}
 */
function ru(name){
    if(name.toLowerCase() === "moi" || name.toLowerCase() === "je") return 'moi';
    else return name;
}

/**
 * Je simule une requête SQL comme ci-après :
 *
 * @param user1
 * @param user2
 * @returns {Promise<>}
 */
function updateDb(user1, user2) {
    return new Promise((resolve, reject) => {
        /**
         * INSERT d'un nouveau champ dans la table User [group:int, direct_friends: array|json]
         */
        if(!user_db.hasOwnProperty(user1)) user_db[user1] = {group: undefined, direct_friends: []};
        if(!user_db.hasOwnProperty(user2)) user_db[user2] = {group: undefined, direct_friends: []};

        /**
         * Update des champs direct_friends des deux utilisateurs
         */
        if(user_db[user1].direct_friends.indexOf(user2.toLowerCase()) === -1) user_db[user1].direct_friends.push(user2);
        if(user_db[user2].direct_friends.indexOf(user1.toLowerCase()) === -1) user_db[user2].direct_friends.push(user1);

        /**
         * Si les deux sont les premiers amis respectifs de chacun, alors création d'un nouveau groupe
         */
        if(user_db[user1].group === undefined && user_db[user2].group === undefined) {
            groups_db.push([user1, user2]);
            user_db[user2].group = groups_db.length - 1;
            user_db[user1].group = groups_db.length - 1;
        }else{
            /**
             * Si l'un des deux a deja des amis, l'autre intègre le groupe du premier.
             */

            if(user_db[user1].group === undefined) {
                user_db[user1].group = user_db[user2].group;
                groups_db[user_db[user2].group].push(user1);
            }
            if(user_db[user2].group === undefined) {
                user_db[user2].group = user_db[user1].group;
                groups_db[user_db[user1].group].push(user2);
            }

            let u1g = user_db[user1].group;
            let u2g = user_db[user2].group;

            /**
             * Si les deux ont des groupes différents, un nouveau groupe est créé intégrant tous les amis des deux groupes.
             * Les deux anciens groupes n'ont plus lieux d'être et sont delete
             *
             * Donc ici :
             *  1 requête INSERT sur la table groupe
             *  1 requête UPDATE sur tous les utilisateurs des anciens groupes pour leur attribuer le nouveau groupe.
             */
            if(u1g !== u2g){
                const new_group = groups_db[u1g].concat(groups_db[u2g]);

                groups_db.splice(u1g,1);
                groups_db.splice(u2g,1);
                groups_db.push(new_group);

                const new_group_index = groups_db.length-1;

                Object.keys(user_db).forEach((user)=>{
                    if(user_db[user].group=== u1g || user_db[user].group=== u2g) user_db[user].group = new_group_index;
                });
            }
        }

        resolve()
    })
}