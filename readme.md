Explications de l'exercice:

**1) Pourquoi pas de DB?**

- J'ai préféré faire une DB maison en quelques minutes, n'impliquant pas de docker ou autre.
- Afin que ça soit lisible comme une DB j'ai ajouté des explications pour faire la comparaison.
    
**3) Quelle méthode ?**

- J'ai opté pour une méthode à 2 tables : 1 table user & 1 table groupe
- La table groupe possède un champs users type Array, elle permet de stocker tous les amis d'un groupe
- Les Groupes peuvent fusionner lorsque deux personnes ayant deja des amis deviennent amis.
   
**2) Pourquoi cette méthode?**

    a) Pas besoins d'orm / Requêtes complexes impliquant des loops ou autre joins
    b) La table user permet de stocker les amitiés directement enregistrées dans le prompt.
    c) Cela donne la possibilité de changer l'algorithme en ayant un set de données neutre.
    d) La requête pour ajouter une amitié demande environ ~ 5 requêtes simples
    e) La requête question est extrêmement simple => comparer les 2 groupes des deux protagonistes
    f) Encore simplifiable, possibilité de supprimer la table groupe 

